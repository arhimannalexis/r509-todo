"""
Création d'une ToDo list pour le TP de R509.
Développeur : Alexis Arhimann
Concepteur : Alexis Arhimann
Date de début : 14/12/2023
Date de rendu du projet : ?

____________________________________________________________________________________
Conception :

Pour faire une ToDo list le plus simple semble être une application web avec flask ou fastapi.

L'application devra proposer une interface d'accueil avec la liste d'affichée, des checkbox pour chaque élément de la lite et deux boutons : un pour ajouter un élément et un pour en supprimer.

Une option d'authentification est envisageable pour avoir plusieurs utilisateurs avec leur propre ToDo list.

Pour le début du développement pas de concentration sur le visuel, intégration de bootstrap si possible pour rendre les pages plus agréables.

Chaque élément de la liste sera su texte stocké dans un fichier. Le fichier sera ensuite lu au chargement de l'URL de la liste pour récupérer les informations et les afficher dans un template jinja2.
"""

from fastapi import FastAPI, Path, Request
from typing import Annotated
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
import json

app = FastAPI()

@app.get("/")
async def home(request: Request):
    element_dict={}
    f = open('list.txt', 'r', encoding="utf-8")
    content = f.read()
    content_split = content.split('\n')
    # print(f"liste splitée : {type(content_split)}")
    f.close()
    count = 0
    for i in content_split:
        if i != "":
            element_dict[count]=i
            count += 1
    element_json = json.dumps(element_dict)
    return element_json

@app.put("/add/{element}")
async def add(element: Annotated[str, Path(description="élément à ajouter")]):
    f = open('list.txt', 'a', encoding="utf-8")
    f.write("\n"+element)
    f.close()
    return 201, "element added successfully"

@app.delete("/delete/{element}")
async def delete(element: Annotated[str, Path(description="élément à supprimer")]):
    f = open('list.txt', 'r', encoding="utf-8")
    content = f.read()
    content_split = content.split('\n')
    f.close()
    for i in content_split:
        if i == element:
            content_split.remove(i)
            f = open('list.txt', 'w', encoding="utf-8")
            for line in content_split:
                f.write(line+"\n")
            f.close()
            return 202, "element deleted successfully"
    else:
        return 510, "l'élément fournit n'existe pas"

import uvicorn
if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=5009)