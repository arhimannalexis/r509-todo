from fastapi import FastAPI, HTTPException, Path, Body, Request
from typing import Union, Callable, Annotated
from pydantic import BaseModel
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
import json, requests, os

app = FastAPI()

templates = Jinja2Templates(directory="Templates")

@app.get("/", response_class=HTMLResponse)
async def display_list(request: Request):
    # api_service_url = 'http://localhost:5000/'
    # test_json = '{"0": "bonjour, test pour le fichier", "1": "deuxi\u00e8me ligne", "2": "troisi\u00e8me ligne", "3": "sortir la poubelle", "4": "test1", "5": "test2", "6": "test3"}'
    api_service_url = os.environ.get("API_URL")
    reponse = requests.get(api_service_url)
    rep_json = reponse.json()
    json_string = json.dumps(rep_json)
    dico = json.loads(json_string)
    dico2 = json.loads(dico)
    # return 'test'
    # print(type(dico))
    print(dico2)
    return templates.TemplateResponse("index.j2", {"request": request, "dico" : dico2})

import uvicorn
if __name__ == '__main__':

    uvicorn.run(app, host='0.0.0.0', port=5010)